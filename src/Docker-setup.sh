#!/bin/sh

mkdir -p \
/srv/src \
/srv/local_manifests \
/srv/userscripts \
/srv/zips \
/srv/logs \
/srv/ccache \
/srv/keys

curl -fsSL https://get.docker.com -o get-docker.sh

sh get-docker.sh

apt-get update
apt-get upgrade

apt install git unzip sshpass tilde
