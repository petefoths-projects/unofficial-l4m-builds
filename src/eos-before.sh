#!/bin/bash

##########################################################
# Apply the Trebuchet fix patch in packages/apps/Trebuchet
#
# This file should be copied to `/srv/userscripts/before.sh`
# before running Docker
##########################################################

cd packages/apps/Trebuchet

git reset --hard

echo ">> [$(date)] Applying the Trebuchet fix patch"

git apply /root/userscripts/eos-Trebuchet-fix.patch

cd ../../../
#########################################################
