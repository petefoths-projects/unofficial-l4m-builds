#!/bin/sh
set -eu

PERSISTENT_STORAGE=/mnt/persistent
LOCAL_STORAGE=/mnt/local

BRANCH="$1"
echo "Building branch $BRANCH"

DEVICES="$2"

# remove trailing/leading comma
DEVICES=$(echo "$DEVICES" | sed 's/,$//;s/^,//')

echo "Building devices $DEVICES"

# remove trailing/leading comma
DEVICES=$(echo "$DEVICES" | sed 's/,$//;s/^,//')

TAG=":latest"

#  
if [ "$#" -gt 2 ]; then
  TAG="$3"
fi

echo "Tag = $TAG"

echo "Building devices $DEVICES"

MY_WORKDIR="$HOME/work/iode"


docker pull "lineageos4microg/docker-lineage-cicd$TAG"
docker run \
    --name "iodeos-$(date +%Y%m%d%I%M)" \
    --cap-add=SYS_ADMIN \
    --security-opt apparmor=unconfined \
    --security-opt seccomp=unconfined \
    -e "PRODUCT=iodeOS" \
    -e "BRANCH_NAME=$BRANCH" \
    -e "USE_CCACHE=true" \
    -e "CCACHE_SIZE=500G" \
    -e "RELEASE_TYPE=UNOFFICIAL" \
    -e "INCLUDE_PROPRIETARY=false" \
    -e "BUILD_OVERLAY=false" \
    -e "CLEAN_AFTER_BUILD=true" \
    -e "SIGN_BUILDS=true" \
    -e "ZIP_SUBDIR=true" \
    -e "LOGS_SUBDIR=true" \
    -e "DELETE_OLD_ZIPS=1" \
    -e "DELETE_OLD_LOGS=1" \
    -e "BOARD_SYSTEM_EXTIMAGE_PARTITION_RESERVED_SIZE=94371840" \
    -e "WITH_GMS=false" \
    -e "MAKE_IMG_ZIP_FILE=false" \
    -e "USER_NAME='Pete Fotheringham'" \
    -e "USER_MAIL='petefoth@e.email'" \
    -v "$PERSISTENT_STORAGE/src:/srv/src" \
    -v "$PERSISTENT_STORAGE/tmp:/srv/tmp" \
    -v "$PERSISTENT_STORAGE/ccache:/srv/ccache" \
    -v "$MY_WORKDIR/zips:/srv/zips" \
    -v "$MY_WORKDIR/logs:/srv/logs" \
    -v "$MY_WORKDIR/keys:/srv/keys" \
    -v "$MY_WORKDIR/manifests:/srv/local_manifests" \
    -v "$MY_WORKDIR/userscripts:/srv/userscripts" \
    -e "DEVICE_LIST=$DEVICES" \
    lineageos4microg/docker-lineage-cicd$TAG
    
    