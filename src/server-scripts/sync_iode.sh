#!/bin/sh
set -eu
HOST="rsync@lineage.microg.org"
zipdir="/home/pete/work/iode/zips"


export DEVICE="$1"

echo  "Resynching IodeOS binaries for $DEVICE"

builds_in_zipdir=$(find "$zipdir/$DEVICE" -maxdepth 2 -type f -name "iode-*$DEVICE".zip | wc -l)

builds_on_download_server=$(ssh -i /home/pete/work/scripts/rsync_key \
       -o UserKnownHostsFile=/home/pete/work/scripts/rsync_known_hosts "$HOST" \
	    find "zips/IodeOS/$DEVICE" -maxdepth 2 -type f -name "iode-*$DEVICE".zip | wc -l)
	
echo "$builds_on_download_server builds on download server"
echo "$builds_in_zipdir builds in zipdir"

# corrent permissions on "-super_empty.img" file
sudo  chmod a+r "$zipdir/$DEVICE/"*

if  [ "$builds_on_download_server" = 1 ] || [ "$builds_on_download_server" = 0 ]; then
    echo "rsyncing without --delete"
    rsync -e "ssh -i \"/home/pete/work/scripts/rsync_key\" \
        -o UserKnownHostsFile=\"/home/pete/work/scripts/rsync_known_hosts\"" \
        --verbose --dirs  --times \
        --include="*.zip" --include="*.sha256sum" --include="*.prop" --include="*.img"\
        --exclude="*" \
        "$zipdir/$DEVICE/" "$HOST:zips/IodeOS/$DEVICE"
#elif [ "$builds_in_zipdir" = 0 ]; then
#    echo "No builds for DEVICE"
else
    echo " rsyncing with --delete"
    rsync -e "ssh -i \"/home/pete/work/scripts/rsync_key\" \
        -o UserKnownHostsFile=\"/home/pete/work/scripts/rsync_known_hosts\"" \
        --verbose --dirs  --times --delete --delete-excluded \
        --include="*.zip" --include="*.sha256sum" --include="*.prop" --include="*.img"\
        --exclude="*" \
        "$zipdir/$DEVICE/" "$HOST:zips/IodeOS/$DEVICE"
fi

