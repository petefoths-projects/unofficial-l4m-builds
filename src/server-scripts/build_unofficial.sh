#!/bin/sh
set -eu

PERSISTENT_STORAGE=/mnt/persistent
LOCAL_STORAGE=/mnt/local

BRANCH="$1"
echo "Building branch $BRANCH"

DEVICES="$2"

# remove trailing/leading comma
DEVICES=$(echo "$DEVICES" | sed 's/,$//;s/^,//')

echo "Building devices $DEVICES"

# remove trailing/leading comma
DEVICES=$(echo "$DEVICES" | sed 's/,$//;s/^,//')

TAG=""

#  
if [ "$#" -gt 2 ]; then
  TAG="$3"
fi

echo "Tag = $TAG"

echo "Building devices $DEVICES"

MY_WORKDIR="$HOME/work/l4m"


docker pull "lineageos4microg/docker-lineage-cicd$TAG"
docker run \
    --name "lineageos4microg-$(date +%Y%m%d%I%M)" \
    --cap-add=SYS_ADMIN \
    --security-opt apparmor=unconfined \
    --security-opt seccomp=unconfined \
    -e "BRANCH_NAME=$BRANCH" \
    -e "USE_CCACHE=true" \
    -e "CCACHE_SIZE=500G" \
    -e "RELEASE_TYPE=microG-UNOFFICIAL" \
    -e "OTA_URL=https://download.lineage.microg.org/api" \
    -e "BUILD_OVERLAY=false" \
    -e "CLEAN_AFTER_BUILD=true" \
    -e "SIGN_BUILDS=true" \
    -e "ZIP_SUBDIR=true" \
    -e "LOGS_SUBDIR=true" \
    -e "SIGNATURE_SPOOFING=no" \
    -e "DELETE_OLD_ZIPS=2" \
    -e "DELETE_OLD_LOGS=2" \
    -e "WITH_GMS=true" \
    -e "MAKE_IMG_ZIP_FILE=true" \
    -e "INCLUDE_PROPRIETARY=falsee" \
    -v "$PERSISTENT_STORAGE/src:/srv/src" \
    -v "$LOCAL_STORAGE/tmp:/srv/tmp" \
    -v "$PERSISTENT_STORAGE/ccache:/srv/ccache" \
    -v "$PERSISTENT_STORAGE/mirror:/srv/mirror" \
    -v "$MY_WORKDIR/zips:/srv/zips" \
    -v "$MY_WORKDIR/logs:/srv/logs" \
    -v "$MY_WORKDIR/l4mkeys:/srv/keys" \
    -v "$MY_WORKDIR/manifests:/srv/local_manifests" \
    -v "$MY_WORKDIR/userscripts:/srv/userscripts" \
    -e "DEVICE_LIST=$DEVICES" \
    "lineageos4microg/docker-lineage-cicd$TAG"
