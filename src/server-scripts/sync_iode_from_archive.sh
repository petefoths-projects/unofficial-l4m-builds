#!/bin/sh

ARCHIVE="rsync@lineage.microg.org"
zipdir="/home/pete/work/iode/zips"

SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")
DEVICE="$1"

ls -l "$zipdir/$DEVICE/"

builds_on_download_server=$(ssh -i /home/pete/work/scripts/rsync_key \
   -o UserKnownHostsFile=/home/pete/work/scripts/rsync_known_hosts "$ARCHIVE" \
    find "zips/IodeOS/$DEVICE" -maxdepth 2 -type f -name "iode-*$DEVICE".zip | wc -l)
	
echo "$builds_on_download_server builds on download server"

if  [ "$builds_on_download_server" -gt 0 ]; then
    echo "synching files from download server"
    sudo rsync -e "ssh -i \"$SCRIPT_PATH/rsync_key\" \
    -o UserKnownHostsFile=\"$SCRIPT_PATH/rsync_known_hosts\"" \
    --verbose --dirs  --times \
    "$ARCHIVE:zips/IodeOS/$DEVICE/" "$zipdir/$DEVICE" 
fi
ls -l "$zipdir/$DEVICE/"

exit
$(dirname $0)/matrix.sh "Starting build for $1"