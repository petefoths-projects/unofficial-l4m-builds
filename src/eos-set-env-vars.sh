#!/bin/bash

#########################################################
# arguments:
# $1 - Device codename
# $2 - Branch name use /e/ naming scheme: v1-q, v1-r (default)
#      v1-s, v1-13
# $3 - Release type: un (default), cu
#
#########################################################
# Process arguments if any
# - set and export the environment variables needed by
#   - vendor/e
# modify common makefile to use vendor/e

echo "Running unofficial-l4m-build/src/eos-set-env-vars.sh"

#########################################################
# Process arguments if any

EOS_DEVICE=""
if [ -n "$1" ] ; then
  EOS_DEVICE=$1
fi
echo "EOS_DEVICE: ${EOS_DEVICE}"
export EOS_DEVICE

EOS_BRANCH_NAME=v1-r
if [ -n "$2" ] ; then
  EOS_BRANCH_NAME=$2
fi
echo "EOS_BRANCH_NAME: ${EOS_BRANCH_NAME}"
export EOS_BRANCH_NAME

EOS_RELEASE_TYPE=UNOFFICIAL
if [ "$3" = "cu" ]; then
  EOS_RELEASE_TYPE=CUSTOM
fi
export EOS_BRANCH_NAME
echo "EOS_RELEASE_TYPE: ${EOS_RELEASE_TYPE}"

EOS_INCLUDE_PROPRIETARY=true
case "$EOS_DEVICE" in
  lilac,sunfish,z3ctc)
    EOS_INCLUDE_PROPRIETARY=false
  ;;
esac
export EOS_INCLUDE_PROPRIETARY
echo "EOS_INCLUDE_PROPRIETARY: ${EOS_INCLUDE_PROPRIETARY}"

#########################################################
# environment variables needed by vendor/e
export EOS_REPO=https://github.com/LineageOS/android.git
export EOS_CLEAN_ZIPDIR=true
export EOS_SIGN_BUILDS=true
export EOS_SIGNATURE_SPOOFING=restricted
export CUSTOM_PACKAGES=''
export EOS_CUSTOM_PACKAGES=''
export EXTENDROM_PACKAGES=''
export EOS_CCACHE_DIR="$PWD/ccache/l4m"
export EOS_CCACHE_SIZE=50G


#########################################################
# modify common makefile to use vendor/e
if grep -q 'vendor/e/config/common.mk' $PWD/vendor/lineage/config/common.mk ; then
  echo "vendor e already enabled in /vendor/lineage/config/common.mk"
else
  echo "enabling vendor/e in /vendor/lineage/config/common.mk"
  echo "\$(call inherit-product-if-exists, vendor/e/config/common.mk)" >> $PWD/vendor/lineage/config/common.mk
fi
#########################################################

