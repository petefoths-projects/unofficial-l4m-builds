#!/bin/bash

#########################################################
# - install the python `requests` module
#
#########################################################

#########################################################
curl -O https://bootstrap.pypa.io/get-pip.py

python get-pip.py

python -m pip install requests

#########################################################
