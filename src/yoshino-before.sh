#!/bin/bash

cd hardware/interfaces
echo "reverting commit 7ad5797272c40c2b83b0df5a57a0f248a210091b in hardware/interfaces"
git revert 7ad5797272c40c2b83b0df5a57a0f248a210091b
cd ../..

cd kernel/configs/
echo "reverting commit 3610907a2b8fd449665d30b09085234e5eb04dd9 in kernel/configs"
git revert 3610907a2b8fd449665d30b09085234e5eb04dd9
cd ../..

cd device/sony/yoshino-common
echo "cherry-picking commit from /aomsin2526/android_device_sony_yoshino-common"
git fetch "https://github.com/aomsin2526/android_device_sony_yoshino-common" lineage-21-qpr3 && git cherry-pick 5cacc64423de10497e3463b0032b0525c2dd3d19
cd ../../..
