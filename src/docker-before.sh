#!/bin/bash

#########################################################
# - modify common makefile to use extendrom (if it's enabled)
# - call the extendrom get_prebuilts script to download packages
#   and modify its makefile
#
#########################################################

#########################################################
# modify common makefile to use vendor/extendrom

if [ "$ENABLE_EXTENDROM" = true ]; then

  if grep -q 'inherit-product-if-exists, vendor/extendrom/config/common.mk' $PWD/vendor/lineage/config/common.mk ; then
    echo "extendrom already enabled in /vendor/lineage/config/common.mk"
  else
    echo "enabling extendrom in /vendor/lineage/config/common.mk"
    echo "\$(call inherit-product-if-exists, vendor/extendrom/config/common.mk)" >> $PWD/vendor/lineage/config/common.mk
  fi

# call the extendrom script to download packages and modify its makefile
  $PWD/vendor/extendrom/get_prebuilts.sh

fi
#########################################################
