#!/bin/sh
set -eu

PERSISTENT_STORAGE=/mnt/persistent
LOCAL_STORAGE=/mnt/local

BRANCH="$1"
echo "Building branch $BRANCH"

DEVICES="$2"

# remove trailing/leading comma
DEVICES=$(echo "$DEVICES" | sed 's/,$//;s/^,//')

# copy all the manifests we have to local_manifests
LOCAL_MANIFESTS_DIR="$PWD"/local_manifests/
MANIFESTS_DIR="$PWD"/manifests/
branch_num=$(echo "$BRANCH" | sed 's/lineage-//')
UNOFFICIAL_MANIFESTS_DIR="$PWD"/manifests/"$branch_num"

rm -f "$PWD"/local_manifests/*
mkdir -p "$PWD"/local_manifests

cp "$MANIFESTS_DIR"/*.xml "$PWD"/local_manifests/
if [ -d "$UNOFFICIAL_MANIFESTS_DIR" ]; then
cp -r "$UNOFFICIAL_MANIFESTS_DIR"/*.xml "$PWD"/local_manifests/
fi

echo "Building devices $DEVICES"

docker run \
    --name "lineageos4microg-$(date +%Y%m%d%I%M)" \
    --cap-add=SYS_ADMIN \
    --security-opt apparmor=unconfined \
    --security-opt seccomp=unconfined \
    -e "BRANCH_NAME=$BRANCH" \
    -e "USE_CCACHE=true" \
    -e "CCACHE_SIZE=500G" \
    -e "RELEASE_TYPE=microG-UNOFFICIAL" \
    -e "OTA_URL=https://download.lineage.microg.org/api" \
    -e "INCLUDE_PROPRIETARY=false" \
    -e "BUILD_OVERLAY=false" \
    -e "CLEAN_OUTDIR=false" \
    -e "CLEAN_AFTER_BUILD=true" \
    -e "SIGN_BUILDS=true" \
    -e "ZIP_SUBDIR=true" \
    -e "LOGS_SUBDIR=true" \
    -e "LOCAL_MIRROR=true" \
    -e "SIGNATURE_SPOOFING=restricted" \
    -e "DELETE_OLD_ZIPS=2" \
    -e "DELETE_OLD_LOGS=2" \
    -e "WITH_GMS=true" \
    -v "$PERSISTENT_STORAGE/src:/srv/src" \
    -v "$LOCAL_STORAGE/tmp:/srv/tmp" \
    -v "$PERSISTENT_STORAGE/ccache:/srv/ccache" \
    -v "$PERSISTENT_STORAGE/mirror:/srv/mirror" \
    -v "$PERSISTENT_STORAGE/zips:/srv/zips" \
    -v "$PERSISTENT_STORAGE/logs:/srv/logs" \
    -v "$HOME/keys:/srv/keys" \
    -v "$HOME/l4m_build_scripts/local_manifests:/srv/local_manifests" \
    -v "$HOME/l4m_build_scripts/userscripts:/srv/userscripts" \
    -e "DEVICE_LIST=$DEVICES" \
    lineageos4microg/docker-lineage-cicd
