#!/bin/bash

#########################################################
# arguments:
# $1 - Device codename
# $2 - Branch name use /e/ naming scheme: v1-q, v1-r (default)
#      v1-s, v1-13
# $3 - Release type: 
#      un (UNOFFICIAL, default), 
#      cu (CUSTOM-PLUS-E)
#      cw (CUSTOM-WiFi)
#
#########################################################
# Process arguments if any
# - set and export the environment variables needed by
#   - vendor/e
#   - vendor/extendrom if we're making a custom builds
# - copy our `before.sh` to userscripts directory

echo "Running unofficial-l4m-build/src/setup-env-vars.sh"

#########################################################
# Process arguments if any

EOS_DEVICE=""
if [ -n "$1" ] ; then
  EOS_DEVICE=$1
fi
echo "EOS_DEVICE: ${EOS_DEVICE}"
export EOS_DEVICE

EOS_BRANCH_NAME=v1-r
if [ -n "$2" ] ; then
  EOS_BRANCH_NAME=$2
fi
echo "EOS_BRANCH_NAME: ${EOS_BRANCH_NAME}"
export EOS_BRANCH_NAME

EOS_RELEASE_TYPE=4microg-UNOFFICIAL
if [ "$3" = "cu" ]; then
  EOS_RELEASE_TYPE=4microg-CUSTOM-PLUS-E
elif [ "$3" = "cw" ]; then
  EOS_RELEASE_TYPE=4microg-CUSTOM-WiFi
fi
echo "EOS_RELEASE_TYPE: ${EOS_RELEASE_TYPE}"
export EOS_RELEASE_TYPE

#########################################################
# environment variables needed by vendor/e
export EOS_REPO=https://github.com/LineageOS/android.git
export EOS_SIGN_BUILDS=true
export EOS_SIGNATURE_SPOOFING=restricted
export CUSTOM_PACKAGES=''
export EOS_CUSTOM_PACKAGES=''
export EXTENDROM_PACKAGES=''
export EOS_CCACHE_DIR="$PWD/ccache/l4m"
export EOS_CCACHE_SIZE=50G
export L4M_APPLY_SS_PATCHES=true

l4m_custom_packages='Lawnchair-latest AuroraStore BlissLauncher Bromite Fennec DAVx5 ICSx5 NextCloud  NextCloudNotes OpenCamera OpenTasks K9-Mail-latest QKSMS noLOSSnap noLOSEmail noLOSMessaging noLOSJelly'

l4m_custom_wifi_packages='AuroraStore Bromite DAVx5 ICSx5 NextCloud  NextCloudNotes K9-Mail-latest noLOSSnap noLOSEmail'

case "$EOS_RELEASE_TYPE" in
  4microg-CUSTOM-PLUS-E)
    export WITH_GMS=true
    export ENABLE_EXTENDROM=true
    export EXTENDROM_PACKAGES=$l4m_custom_packages
  ;;

  4microg-CUSTOM-WiFi)
    export WITH_GMS=true
    export ENABLE_EXTENDROM=true
    export EXTENDROM_PACKAGES=$l4m_custom_wifi_packages
  ;;

  4microg-UNOFFICIAL)
  export WITH_GMS=true
  export ENABLE_EXTENDROM=false
  ;;

  *)
  # 'vanilla' LinegaOS - no microG
  export WITH_GMS=false
  export ENABLE_EXTENDROM=false
  export L4M_APPLY_SS_PATCHES=false
  ;;

esac

########################################################
# copy before.sh to userscripts directory (.e/userscripts/)
userscripts_dir="${PWD}/.e/userscripts"
mkdir -p $userscripts_dir

chmod a+x  ${PWD}/l4m-builds/src/before.sh
cp ${PWD}/l4m-builds/src/before.sh $userscripts_dir
chmod a+x  $userscripts_dir/before.sh
