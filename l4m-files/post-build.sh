#!/bin/sh

HOST="rsync@lineage.microg.org"

SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")
DEVICE="$1"
BUILD_SUCCESSFUL="$2"

if [ "$BUILD_SUCCESSFUL" = true ]; then
  # Copy every file with .zip, .md5sum, .prop or .sha256sum extension.
  # Do not recurse into directories and remove from the destination every file
  # not present in the source or not ending with the allowed extensions.
  # Keep the original modification timestamp.
  builds_in_zipdir=$(find /srv/zips -maxdepth 2 -type f -name "lineage-*$DEVICE".zip | wc -l)

  if [ "$builds_in_zipdir" = 1 ]; then
      echo "Only one build - rsyncing without --delete"
      rsync -e "ssh -i \"$SCRIPT_PATH/rsync_key\" \
          -o UserKnownHostsFile=\"$SCRIPT_PATH/rsync_known_hosts\"" \
    --verbose --dirs  --times \
    --include="*.zip" --include="*.sha256sum" --include="*.prop" --include="*.img"\
    --exclude="*" \
    "/srv/zips/$DEVICE/" "$HOST:zips/$DEVICE"
  else
      echo "$builds_in_zipdir builds - rsyncing with --delete"
      rsync -e "ssh -i \"$SCRIPT_PATH/rsync_key\" \
          -o UserKnownHostsFile=\"$SCRIPT_PATH/rsync_known_hosts\"" \
    --verbose --dirs  --times --delete --delete-excluded \
    --include="*.zip" --include="*.sha256sum" --include="*.prop" --include="*.img"\
    --exclude="*" \
    "/srv/zips/$DEVICE/" "$HOST:zips/$DEVICE"
  fi
$(dirname $0)/matrix.sh "Completed build for $1"
else
  $(dirname $0)/matrix.sh "Failed build for $1"
fi
