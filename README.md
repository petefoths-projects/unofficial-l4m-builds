# Unofficial builds of LineageOS for microG
For background about this project, including details of the contents of the `UNOFFICIAL` and `CUSTOM-PLUS-E` please see [the project Wiki](https://codeberg.org/petefoths-projects/unofficial-l4m-builds/wiki/Home)

This project was originally hosted on GitHub, but everything except the Discussions (which are not yet supported by Gitea/Codeberg) has now migrated here.

## Feedback - Comments / Issues

I'm currently using [Github Discussions](https://github.com/petefoth/unofficial-l4m-builds/discussions/) as a means to provide feedback - comments, issues, suggestions. Please keep contributions civil and relevant. I will remove any that I decide are not :). When gitea/ and Codeberg are able to host discussions (or a similar feature), I will migrate the github discussions here

If you have found something that you think is a bug, please feel free to [create an issue here](https://codeberg.org/petefoths-projects/unofficial-l4m-builds/issues)

## Building the ROMs

*To do:*
- *link to my manifests and build script*
- *links to my `extendrom` fork repo and the upstream*

## The builds

The builds can be downloaded from [my AndroidFileHost account](https://androidfilehost.com/?w=devices&uid=4349826312261814240)

### For Sony devices
**Z5 Compact (suzuran)**

The latest builds will always be available in [this directory](https://www.androidfilehost.com/?w=files&flid=322410)

**XZ1 Compact (lilac)**

The latest builds will always be available in [this directory](https://androidfilehost.com/?w=files&flid=322414)

**Z3 Tablet Compact (z3tc)**

The latest builds will always be available in [this directory](https://www.androidfilehost.com/?w=files&flid=335191)

### For devices from Other Manufacturers

**Samsung Galaxy Tab S3 (gts3llte gts3lwifi)**

The latest builds will always be available in [this directory](https://androidfilehost.com/user/?w=settings-dev-files&flid=336062)